<?php

/**
 * App Constants Config
 */
return [
    'message' => [
        'message_not_ok' => 0,
        'message_ok'     => 1,
    ],
    'test'    => [
        'test_waiting'   => 0,
        'test_success'   => 1,
        'test_failing'   => 2,
    ]
];
