<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Страница презентации
 */

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return Redirect::route('testList');
} /*'Represent\HomeController@home'*/)->name('RepresentHome');

/**
 * Пользователь
 */
Route::get('/signin', 'User\LoginController@signin')->middleware('guest')->name('signIn');
Route::get('/signup', 'User\RegisterController@register')->middleware('guest')->name('signUp');
Route::get('/forgot', 'User\RegisterController@forgot')->middleware('guest')->name('forgot');

/**
 * Пользователь Requests
 */
Route::post('/auth/login', 'Auth\LoginController@auth')->name('loginControl');

/**
 * Главные страницы
 */
Route::get('/dashboard', 'General\DashboardController@index')->middleware('auth')->name('dashboard');

/**
 * Сайт
 */
Route::prefix('site')->group(function () {
    Route::get('/', 'Site\SiteController@index')->name('site');
    Route::post('/create', 'Site\SiteController@create')->name('siteCreate');
});

/**
 * Тестирование
 */
Route::prefix('test')->group(function () {
    Route::get('/', 'Test\TestController@index')->name('test');
    Route::get('/init', 'Test\TestController@init')->name('testInit');
    Route::get('/experts', 'Test\ExpertsController@experts')->name('testExperts');
    Route::get('/list', 'Test\ListController@list')->name('testList');

    Route::post('/testing', 'Test\TestTryController@testing')->name('testing');

    /*
     * ajax
     */
    Route::post('/checkstatus/{id}', 'Test\AjaxTest\CheckTestStatusController@checkstatus')->name('checkTest');
    Route::post('/fixresult/{id}', 'Test\AjaxTest\FixResultController@fixresult')->name('fixTest');
    Route::post('/addnewdata/{id}', 'Test\AjaxTest\FixResultController@addnewdata')->name('addTestData');
});

/**
 * Вычислительный раздел
 */
Route::get('/calc/scopes', 'Calc\ScopeController@scopes')->name('calcScopes');
