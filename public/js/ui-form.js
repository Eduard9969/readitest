(function ($) {
	"use strict";

	$(document).on('blur', 'input, textarea', function(e){
		$(this).val() ? $(this).addClass('has-value') : $(this).removeClass('has-value');
	});

	$(document).on('pjaxEnd load')
    {
        $('input.quantity').bind("change keyup input click", function() {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });

        jQuery('#update-test-data').on('submit', function (e) {
            e.preventDefault();

            let main_body   = jQuery('body'),
                test_id     = main_body.find('input[name=test_id]').val(),
                token       = main_body.find('input[name=_token]').val();

            if (test_id === undefined || token === undefined)
                return false;

            $.ajax({
                url: 'test/addnewdata/' + test_id,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                global: false,
                data: $(e.target).serialize(),
                type: 'POST',
                success: function (json) {
                    var code = MESSAGE_NO;
                    if(json.status === 1) code = MESSAGE_OK;

                    if (json.message) {
                        send_notification(json.message, code);

                        if(code === MESSAGE_OK && json.site_id) {
                            updateTest(site_id, token, true);
                            displayAddonsPanel(true);
                        }
                    }
                }
            });
        });
    }

	$(document).on('pjaxEnd load')
    {
        var main_body = jQuery('body');
        var start_test_flag = main_body.find('input[name=runtest]');

        if (start_test_flag.length > 0)
        {
            var site_id = start_test_flag.val(),
                token   = main_body.find('input[name=_token]').val();

            updateTest(site_id, token);
        }
    }

    function updateTest(site_id, token, update) {
        var main_body = jQuery('body');

        if(site_id === undefined || token === undefined)
            return false;

        if (update === undefined) update = false;

        var timeout = setInterval(function () {
            $.ajax({
                url: 'test/checkstatus/' + site_id,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                global: false,
                type: 'POST',
                success: function (json) {
                    if(json.status === 1)
                    {
                        if (json.result)
                        {
                            jQuery.each(json.result, function (k, i) {
                                main_body.find('[data-input-value=' + k + ']').html(i + (i > 0 ? '<small class="text-muted">%</small>' : ''));
                            });

                            if (json.test_id)
                            {
                                var input_field = "<input type='hidden' name='test_id' value='" + json.test_id + "'>";
                                main_body.append(input_field);
                            }
                        }

                        main_body.find('.text-preview-test').css('visibility', 'hidden');

                        if(!update) displayAddonsPanel();
                        main_body.find('.text-preview-btn').fadeIn();
                    }

                    if (json.is_final) clearInterval(timeout);
                }
            });
        }, 2000);
    }

    function displayAddonsPanel(hide) {
        if (hide === undefined) hide = false;

        var block = jQuery('div[data-action=post-testing]');

        if (hide) block.slideUp();
        else block.slideDown();
    }

})(jQuery);
