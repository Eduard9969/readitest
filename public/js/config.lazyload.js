// lazyload config

var root_path     = location.protocol + '//' + location.host + '/';

var MODULE_CONFIG = {
    screenfull:     [
                      root_path + 'libs/screenfull/dist/screenfull.min.js'
                    ],
    stick_in_parent:[
                      root_path + 'libs/sticky-kit/jquery.sticky-kit.min.js'
                    ],
    owlCarousel:    [
                      root_path + 'libs/owl.carousel/dist/assets/owl.carousel.min.css',
                      root_path + 'libs/owl.carousel/dist/assets/owl.theme.css',
                      root_path + 'libs/owl.carousel/dist/owl.carousel.min.js'
                    ],
    easyPieChart:   [ root_path + 'libs/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ root_path + 'libs/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ root_path + 'libs/flot/jquery.flot.js',
                      root_path + 'libs/flot/jquery.flot.resize.js',
                      root_path + 'libs/flot/jquery.flot.pie.js',
                      root_path + 'libs/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      root_path + 'libs/flot-spline/js/jquery.flot.spline.min.js',
                      root_path + 'libs/flot.orderbars/js/jquery.flot.orderBars.js'
                    ],
    echarts:        [
                      root_path + 'libs/echarts/build/dist/echarts-all.js',
                      root_path + 'libs/echarts/build/dist/theme.js',
                      root_path + 'libs/echarts/build/dist/jquery.echarts.js'
                    ],
    chart:          [
                      root_path + 'libs/chart.js/dist/Chart.bundle.min.js',
                      root_path + 'libs/chart.js/dist/jquery.chart.js'
                    ],
    vectorMap:      [ root_path + 'libs/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      root_path + 'libs/bower-jvectormap/jquery-jvectormap.css',
                      root_path + 'libs/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      root_path + 'libs/bower-jvectormap/jquery-jvectormap-us-aea-en.js'
                    ],
    dataTable:      [
                      root_path + 'libs/datatables/media/js/jquery.dataTables.min.js',
                      root_path + 'libs/datatables/media/js/dataTables.bootstrap4.min.js',
                      root_path + 'libs/datatables/media/css/dataTables.bootstrap4.min.css',
                      root_path + 'libs/datatables/extensions/buttons/dataTables.buttons.min.js',
                      root_path + 'libs/datatables/extensions/buttons/buttons.bootstrap4.min.js',
                      root_path + 'libs/datatables/extensions/buttons/jszip.min.js',
                      root_path + 'libs/datatables/extensions/buttons/pdfmake.min.js',
                      root_path + 'libs/datatables/extensions/buttons/vfs_fonts.js',
                      root_path + 'libs/datatables/extensions/buttons/buttons.html5.min.js',
                      root_path + 'libs/datatables/extensions/buttons/buttons.print.min.js',
                      root_path + 'libs/datatables/extensions/buttons/buttons.colVis.min.js'
                    ],
    footable:       [
                      root_path + 'libs/footable/dist/footable.all.min.js',
                      root_path + 'libs/footable/css/footable.core.css'
                    ],
    sortable:       [
                      root_path + 'libs/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      root_path + 'libs/nestable/jquery.nestable.css',
                      root_path + 'libs/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      root_path + 'libs/summernote/dist/summernote.css',
                      root_path + 'libs/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      root_path + 'libs/parsleyjs/dist/parsley.css',
                      root_path + 'libs/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      root_path + 'libs/select2/dist/css/select2.min.css',
                      root_path + 'libs/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      root_path + 'libs/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      root_path + 'libs/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      root_path + 'libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      root_path + 'libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      root_path + 'libs/moment/moment.js',
                      root_path + 'libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    bootstrapWizard:[
                      root_path + 'libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      root_path + 'libs/moment/moment.js',
                      root_path + 'libs/fullcalendar/dist/fullcalendar.min.js',
                      root_path + 'libs/fullcalendar/dist/fullcalendar.css',
                      root_path + 'libs/fullcalendar/dist/fullcalendar.theme.css',
                      root_path + 'scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      root_path + 'libs/dropzone/dist/min/dropzone.min.js',
                      root_path + 'libs/dropzone/dist/min/dropzone.min.css'
                    ]
  };

// jQuery plugin default options
var JP_CONFIG = {};
