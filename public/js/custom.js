const MESSAGE_OK   = 1;
const MESSAGE_NO   = 2;
const MESSAGE_WAIT = 3;

let fixtestresult = function() {
    let test = jQuery('body input[name=test_id]');

    if (test.length === 0) {
        send_notification('Произошла ошибка при добавлении', MESSAGE_NO);
        return false;
    }

    let test_id = test.val(),
        token   = jQuery('body').find('input[name=_token]').val();

    if (test_id === undefined || test_id === '' || test_id === null) {
        send_notification('Произошла ошибка при добавлении', MESSAGE_NO);
        return false;
    }

    $.ajax({
        url: 'test/fixresult/' + test_id,
        headers: {
            'X-CSRF-TOKEN': token
        },
        global: false,
        type: 'POST',
        success: function (json) {
            var code = MESSAGE_NO;
            if(json.status === 1) code = MESSAGE_OK;

            if (json.message) {
                send_notification(json.message, code);

                if(code === MESSAGE_OK) {
                    jQuery('body').find('.btn-comp').parent().show();
                    setTimeout(function () {
                        jQuery('body').find('.btn-comp').parent().hide();
                    }, 3000);
                }
            }
        }
    });

}

let copylink = function() {
    send_notification('Уникальная ссылка скопирована');
}

let refreshtest = function () {
    send_notification('На данный момент недоступен сброс кеша. Выполняется сортировка очереди', MESSAGE_WAIT);
}

function send_notification(message, type)
{
    if (type === undefined) type = MESSAGE_OK;

    var color = (type === MESSAGE_OK ? 'success' :
                    (type === MESSAGE_NO ? 'danger' : 'warning'));

    var alert = '<div class="box-color ' + color + ' pos-rlt pointer" onclick="this.remove()">' +
        '<span class="arrow right b-' + color + '"></span>' +
        '<div class="box-body p-y-1">' + message + '</div>' +
        '</div>';
    jQuery('body').find('.notification_area').append(alert);
}
