<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class RunTestSite implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $site_id;

    /**
     * Create a new job instance.
     *
     * @param $site_id
     */
    public function __construct($site_id)
    {
        $this->site_id = $site_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if(!empty($this->site_id))
        {
            $client = new Client();
            $url    = URL::to('/test/testing/');

            $request = $client->request('POST', $url, [
                'headers' => [
                    'X-CSRF-TOKEN' => csrf_token()
                ],
                'form_params' => [
                    'site_id'     => $this->site_id,
                    'secret_code' => config('api.app_secret_key')
                ]
            ]);

            $status_id = $request->getStatusCode();

            $string = 'RUN test ' . $url . '/' . $this->site_id . ' . Status Request: ' . $status_id;
            Log::info($string);
        }
    }
}
