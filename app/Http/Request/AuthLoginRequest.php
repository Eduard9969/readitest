<?php

namespace App\Http\Request;

use Dotenv\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AuthLoginRequest
 */
class AuthLoginRequest extends FormRequest
{

    /**
     * Авторизация
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Модификация данных
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if (isset($data['email']))
        {
            $validate = \Illuminate\Support\Facades\Validator::make($data, ['email' => 'required|min:5|max:255']);

            if(!$validate->fails())
            {
                $validate = \Illuminate\Support\Facades\Validator::make($data, ['email' => 'email']);

                if($validate->fails())
                {
                    $data['login'] = $data['email'];
                    unset($data['email']);
                }
            }
        }
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }

    /**
     * Правила валидации данных
     *
     * @return array
     */
    public function rules()
    {
        $rules['password'] = 'required|min:5|max:255';

        $data = $this->all();
        if(isset($data['email']))
            $rules['email'] = 'required|email|min:5';
        else
            $rules['login'] = 'required|min:3|max:255';

        return $rules;
    }

    /**
     * Сообщения ошибок
     *
     * @return array
     */
    public function messages()
    {
        $required_email  = 'Email или логин обязательный!';
        $required_pass   = 'Пароль обязательный!';

        $not_valid_email = 'Некорректный email или логин';
        $not_valid_pass  = 'Некорректный пароль';

        $messages = [
            'email.required'    => $required_email,
            'email.email'       => $not_valid_email,
            'email.min'         => $not_valid_email,

            'login.required'    => $required_email,
            'login.min'         => $not_valid_email,
            'login.max'         => $not_valid_email,

            'password.required' => $required_pass,
            'password.min'      => $not_valid_pass,
            'password.max'      => $not_valid_pass,
        ];

        return $messages;
    }
}
