<?php


namespace App\Http\Request;


use Illuminate\Foundation\Http\FormRequest;

class SiteRequest extends FormRequest
{
    /**
     * Авторизация
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила валидации данных
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|url'
        ];
    }

    /**
     * Сообщения ошибок
     *
     * @return array
     */
    public function messages()
    {
        return [
            'url.required' => 'Url не может быть пустым!',
            'url.min'      => 'Слишком короткий url',
            'url.url'      => 'Некоректный url'
        ];
    }

}
