<?php


namespace App\Http\Repositories;


use App\Http\Models\Test\Test;

class TestRepository
{
    /**
     * @var Site
     */
    protected $testModel;

    /**
     * SiteRepository constructor.
     * @param Test $testModel
     */
    public function __construct(Test $testModel)
    {
        $this->testModel = $testModel;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        return $this->testModel->create($attributes);
    }


    /**
     * @return array
     */
    public function getAll()
    {
        return $this->testModel->getList();
    }

    /**
     * @return array
     */
    public function getAllExperts()
    {
        return $this->testModel->getListExperts();
    }


    /**
     * @param $site_id
     * @return \Illuminate\Support\Collection
     */
    public function getBySiteId($site_id)
    {
        return $this->testModel->getBySiteId($site_id);
    }

    /**
     * @param $test_id
     * @param $params
     * @return int
     */
    public function UpdateByid($test_id, $params)
    {
        return $this->testModel->UpdateByid($test_id, $params);
    }

    /**
     * @param $test_id
     * @return \Illuminate\Support\Collection
     */
    public function getById($test_id)
    {
        return $this->testModel->getById($test_id);
    }

}
