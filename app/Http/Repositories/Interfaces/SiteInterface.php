<?php


namespace App\Http\Repositories\Interfaces;

/**
 * Interface SiteInterface
 * @package App\Http\Repositories\Interfaces
 */
interface SiteInterface
{

    public function create($attributes);

    public function getAll();

    public function getByUrl($url);

    public function getById($id);
}
