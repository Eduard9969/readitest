<?php


namespace App\Http\Repositories\Interfaces;

/**
 * Interface LoginInterface
 * @package App\Http\Repositories\Interfaces
 */
interface LoginInterface
{
    public function getUserByLogin(string $login);

    public function getUserByEmail(string $email);

}
