<?php


namespace App\Http\Repositories;

use App\Http\Models\Test\SimilarTest;

class SimilarTestRepository
{
    protected $similarTestModel;

    public function __construct(SimilarTest $similarTest)
    {
        $this->similarTestModel = $similarTest;
    }

    public function create($values)
    {
        return $this->similarTestModel->create($values);
    }
}
