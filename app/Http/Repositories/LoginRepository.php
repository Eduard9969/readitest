<?php


namespace App\Http\Repositories;

use App\Http\Models\User;
use App\Http\Repositories\Interfaces\LoginInterface;

/**
 * Class LoginRepository
 * @package App\Http\Repositories
 */
class LoginRepository implements LoginInterface
{

    /**
     * Объект модели
     * @var
     */
    protected $loginModel;

    /**
     * LoginRepository constructor.
     *
     * @param User $loginModel
     */
    public function __construct(User $loginModel)
    {
        $this->loginModel = $loginModel;
    }

    /**
     * Получения пользователя по логину
     *
     * @param $login
     * @return string
     */
    public function getUserByLogin(string $login)
    {
        return $this->loginModel->getUserByLogin($login);
    }

    /**
     * Получения пользователя по Email
     *
     * @param string $email
     * @return string
     */
    public function getUserByEmail(string $email)
    {
        return $this->loginModel->getUserByEmail($email);
    }

}
