<?php


namespace App\Http\Repositories;


use App\Http\Models\Site\Site;
use App\Http\Repositories\Interfaces\SiteInterface;

/**
 * Class SiteRepository
 * @package App\Http\Repositories
 */
class SiteRepository implements SiteInterface
{

    /**
     * @var Site
     */
    protected $siteModel;

    /**
     * SiteRepository constructor.
     * @param Site $siteModel
     */
    public function __construct(Site $siteModel)
    {
        $this->siteModel = $siteModel;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        return $this->siteModel->create($attributes);
    }

    /**
     * @return Site[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->siteModel->getList();
    }

    /**
     * @param $url
     * @return \Illuminate\Support\Collection
     */
    public function getByUrl($url)
    {
        return $this->siteModel->getByUrl($url);
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getById($id)
    {
        return $this->siteModel->getById($id);
    }
}
