<?php

namespace App\Http\Services\Math;

/**
 * Class LogicScore
 */
class LogicScore
{
    /*
     * Массив входных данных
     */
    private $inputs = [];

    /*
     * Выходной параметр
     */
    private $output;

    /*
     * Массив ошибок
     */
    private $errors = [];

    /*
     * Выходной параметр в именовании
     */
    private $output_names = [
        'Плохо',
        'Нормально',
        'Отлично'
    ];

    public $min = 30;
    public $max = 70;

    /**
     * LogicScore constructor.
     */
    public function __construct() { }

    /**
     * Установка входных данных
     * @param $key
     * @param $params
     * @return mixed
     */
    public function setParams($key, $params)
    {
        return $this->inputs[$key] = $params;
    }

    /**
     * Проверка на полноту входных данных
     * @return bool
     */
    private function isFullInput()
    {
        return (bool) $this->inputs;
    }

    /**
     * Валидация входных данных
     * @return array|null
     */
    private function validateInput()
    {
        if (!$this->isFullInput())
            return $this->abortError('Empty Input Data');

        $result = [];
        foreach ($this->inputs as $key => $input)
        {
            if(!is_array($input)) {
                unset($this->inputs[$key]);
                continue;
            }

            $true_param = 0;
            foreach ($input as $k => $item)
            {
                if(!is_bool($item))
                {
                    unset($input[$k]);
                }

                $true_param += ($item ? 1 : 0);
            }

            if(empty($input))
            {
                unset($this->inputs[$key]);
                continue;
            }

            $result[$key] = [
                'count'      => count($input),
                'count_true' => $true_param
            ];
        }

        if(empty($result))
            return $this->abortError('Not Valid Input Data');

        return $result;
    }

    /**
     * Вычисления выходного массива
     * @return mixed
     */
    private function calculationOutput()
    {
        $input = $this->validateInput();
        if (empty($input)) return $input;

        $global = [
            'input'         => count($input),
            'input_percent' => 100/count($input)
        ];

        $params = [];
        foreach ($input as $key => $param)
        {
            $values      = $param['count'];
            $values_true = $param['count_true'];

            /*
             * Число признаков входного параметра
             */
            $params[$key]['part'] = $values;

            /*
             * Доля признака во входном параметре, %
             */
            $params[$key]['part_percent']   = $values > 0 ? 100 / $values : 0;

            /*
             * Число признаков входного параметра с флагом true
             */
            $params[$key]['part_correct']   = $values_true;

            /*
             * Доля правильных ответов во входном параметре, %
             */
            $params[$key]['part_to_key']    = round($params[$key]['part_correct'] * $params[$key]['part_percent'], 2);

            /*
             * Доля входного параметра в глобальном счете, %
             */
            $params[$key]['part_to_global'] = $params[$key]['part_to_key'] > 0 ? $global['input_percent'] / 100 * $params[$key]['part_to_key'] : 0;
        }

        $this->output = 0;
        foreach ($params as $param)
        {
            $this->output += isset($param['part_to_global']) ? round($param['part_to_global'], 1) : 0;
        }

        return $this->output;
    }

    /**
     * Получения выходного критерия
     * @return mixed
     */
    public function getOutput()
    {
        return $this->calculationOutput();
    }

    /**
     * Получения наименования выходного критерия
     * @return mixed|null
     */
    public function getOutputName()
    {
        if($this->hasError())
            return $this->abortError('Not has output calc value. Start getOutput');

        $output_index = $this->output < $this->min ? 0 :
                            (($this->output >= $this->min && $this->output < $this->max) ? 1 : 2);

        return $this->output_names[$output_index];
    }

    /**
     * Проверка на наличие ошибки
     * @return bool
     */
    public function hasError()
    {
        return (bool) $this->errors;
    }

    /**
     * Указание ошибки
     * @param $message
     * @return mixed
     */
    private function setError($message)
    {
        return $this->errors[] = $message;
    }

    /**
     * Получения ошибок
     * @return array|null
     */
    public function getErrors()
    {
        return $this->hasError() ? $this->errors : null;
    }

    /**
     * Выход с ошибкой
     * @param $message
     * @return null
     */
    private function abortError($message)
    {
        $this->setError($message);

        return null;
    }

}

/**
 * Data
 * [input_key_1] => [
 *      [param_key_1] => {value|bool},
 *      [param_key_2] => {value|bool},
 *      ...
 *      [param_key_N] => {value|bool}
 * ],
 * [input_key_2] => [
 *      ...
 * ],
 * ...
 * [input_key_N] => [
 *      ...
 * ]
 *
 * INPUT
 *  INPUT_PARAM
 *      INPUT PARAM VALUE - bool type
 */
