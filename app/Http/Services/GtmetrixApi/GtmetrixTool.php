<?php


namespace App\Http\Services\GtmetrixApi;

use App\Http\Services\ApiControl;
use App\Http\Services\ApiControlInterface;
use Entrecore\GTMetrixClient\GTMetrixClient;
use Entrecore\GTMetrixClient\GTMetrixException;

/**
 * Class GtmetrixTool
 * @package App\Http\Services\GtmetrixApi
 */
class GtmetrixTool extends ApiControl implements ApiControlInterface
{

    private $api_key = '';
    private $client;

    /**
     * GtmetrixTool constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setMainFrame(self::GTMERTIX_FRAME);

        $this->api_key = $this->getApiKey();
        $this->client  = new GTMetrixClient();

        $this->init();
    }

    /**
     * Initialization
     */
    private function init()
    {
        $this->client->setUsername($this->getUserName());
        $this->client->setAPIKey($this->api_key);

        $this->checkApi();
    }

    /**
     * Check Api Status
     */
    public function checkApi()
    {
        return $this->api_status = true;
    }

    /**
     * Run Testing
     *
     * @param $url
     * @return \Entrecore\GTMetrixClient\GTMetrixTest|null
     */
    public function testRun($url)
    {
        try {
            return $this->client->startTest($url);
        } catch (GTMetrixException $e) {
            $this->setError($e);
        }

        return null;
    }

    /**
     * Check Test Status
     *
     * @param $test_id
     * @return \Entrecore\GTMetrixClient\GTMetrixTest
     */
    public function checkTestStatus($test_id)
    {
        return $this->client->getTestStatus($test_id);
    }

    /**
     * Return Test Location
     *
     * @return \Entrecore\GTMetrixClient\GTMetrixLocation[]
     * @throws \Entrecore\GTMetrixClient\GTMetrixConfigurationException
     */
    public function getLocations()
    {
        return $this->client->getLocations();
    }

    /**
     * Return Test Browsers
     *
     * @return \Entrecore\GTMetrixClient\GTMetrixBrowser[]
     * @throws \Entrecore\GTMetrixClient\GTMetrixConfigurationException
     */
    public function getBrowsers()
    {
        return $this->client->getBrowsers();
    }
}
