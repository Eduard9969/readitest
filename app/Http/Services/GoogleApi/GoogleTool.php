<?php


namespace App\Http\Services\GoogleApi;

use App\Http\Services\ApiControl;
use App\Http\Services\ApiControlInterface;
use Google_Client;
use Google_Service_Pagespeedonline as Pagespeed;

/**
 * Class GoogleTool
 * @package App\Http\Services\GoogleApi
 */
class GoogleTool extends ApiControl implements ApiControlInterface
{
    private $api_key = '';
    private $client;

    /**
     * GoogleTool constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setMainFrame(self::GOOGLE_FRAME);

        $this->api_key = $this->getApiKey();
        $this->client  =  new Google_Client();

        $this->init();

    }

    public function init()
    {
        $this->client->setApplicationName($this->getAppName());
        $this->client->setDeveloperKey($this->api_key);
//        $this->client->setAccessType('offline');

        if (!$this->checkApi()) {
            $this->setError('Not Valid Api Key');
            return false;
        }
    }

    /**
     * Check Api status
     */
    public function checkApi()
    {
        return $this->api_status = true;
    }

    /**
     * Run Test Url
     *
     * @param $url
     * @return \Google_Service_Pagespeedonline_PagespeedApiPagespeedResponseV5|null
     */
    public function testRun($url)
    {
        $pagespeed = new Pagespeed($this->client);
        try {
            return $this->api_status ? $pagespeed->pagespeedapi->runpagespeed($url) : null;
        } catch (\Google_Service_Exception $e)
        {
            $this->setError($e);
        }

        return null;
    }

    /**
     * UnBase64 Img
     * @param $data
     * @return mixed
     */
    private function unbaseImg($data)
    {
        return str_replace(array('_', '-'), array('/', '+'), $data);
    }
}
