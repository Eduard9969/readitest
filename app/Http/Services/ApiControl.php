<?php

namespace App\Http\Services;

/**
 * Class ApiControl
 * @package App\Http\Services
 */
class ApiControl
{
    const GTMERTIX_FRAME = 1;
    const GOOGLE_FRAME = 2;

    protected $api_status  = false;

    private $main_frame = 0;
    private $errors     = [];

    /**
     * ApiControl constructor.
     */
    public function __construct() { }

    /**
     * Set Main Frame
     *
     * @param $frame_id
     * @return mixed
     */
    public function setMainFrame($frame_id)
    {
        return $this->main_frame = $frame_id;
    }

    /**
     * Get Api Key
     *
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getApiKey()
    {
        $api_key = '';

        if ($this->main_frame == self::GTMERTIX_FRAME)
            $api_key = config('api.gtmetrix_key');
        elseif ($this->main_frame == self::GOOGLE_FRAME)
            $api_key = config('api.google_key');

        return $api_key;
    }

    /**
     * Get User Name
     *
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getUserName()
    {
        $username = '';

        if ($this->main_frame == self::GTMERTIX_FRAME)
            $username = config('api.gtmetrix_user');

        return $username;
    }

    /**
     * Get App Name
     *
     * @return \Illuminate\Config\Repository|mixed|string
     */
    public function getAppName()
    {
        $app_name = '';

        if($this->main_frame == self::GOOGLE_FRAME)
            $app_name = config('api.google_app_name');

        return $app_name;
    }

    /**
     * Set Error
     *
     * @param $error
     * @return mixed
     */
    protected function setError($error)
    {
        return $this->errors[] = $error;
    }

    /**
     * Has Errors
     * @return bool
     */
    public function hasError()
    {
        return (bool) $this->errors;
    }

    /**
     * Get Errors List
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}

/**
 * Interface ApiControlInterface
 * @package App\Http\Services
 */
interface ApiControlInterface
{
    public function checkApi();
}
