<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use phpDocumentor\Reflection\Types\Boolean;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Auth User Id
     * @var int|null
     */
    protected $user_id = 0;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        /*
         * Авторизованный пользователь, id
         */
        $this->user_id = Auth::id();

        /*
         * Глобальные переменные вида
         */
        $this->setGlobalView();
    }

    /**
     * Глобальные переменные вида
     * @return bool
     */
    public function setGlobalView() : bool
    {
        self::setViewAssign('logo', 'ReadiTest'); // Лого
        self::setViewAssign('logo_title', 'Автоматизированная система тестирования ReadiTest'); // Лого описание

        self::setViewAssign('session', Session::class); // Объект класса сессии

        return true;
    }

    /**
     * Простая форма назначения переменной вида
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    protected static function setViewAssign($key, $value)
    {
        return \Illuminate\Support\Facades\View::share($key, $value);
    }

    /**
     * Присвоение переменной в тело шаблона
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    protected function _assign($key, $value)
    {
        return self::setViewAssign($key, $value);
    }
}
