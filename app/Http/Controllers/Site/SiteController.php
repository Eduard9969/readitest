<?php


namespace App\Http\Controllers\Site;

use App\Http\Repositories\SiteRepository;
use App\Http\Request\SiteRequest;
use GuzzleHttp\Client;
use http\Env\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SiteController
 * @package App\Http\Controllers\Site
 */
class SiteController extends BaseSiteController
{

    /**
     * SiteController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SiteRepository $siteRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SiteRepository $siteRepository)
    {
        return view('site.page_create');
    }

    /**
     * @param SiteRepository $siteRepository
     * @param SiteRequest $site
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(SiteRepository $siteRepository, SiteRequest $site)
    {
        $url  = $site->post('url');
        $site = $siteRepository->getByUrl($url);

        if ($site->isEmpty())
        {
            $response = (new Client())->request('GET', $url);
            $code = $response->getStatusCode();

            if($code != 200)
            {
               return Redirect::back()
                        ->withInput(['url' => $url])
                            ->withErrors(['Произошла ошибка при коннекте с сайтом!']);
            }

            $url_parse = parse_url($url);
            $id = $siteRepository->create([
                'url'        => $url,
                'name'       => ucfirst($url_parse['host']),
                'project_id' => 1
            ]);
        }
        else {
            $site = $site->toArray();
            $id = $site[0]->id;
        }

        return Redirect::route('test')->with(['site_id' => $id]);
    }
}
