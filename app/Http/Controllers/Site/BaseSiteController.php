<?php


namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

/**
 * Class BaseSiteController
 * @package App\Http\Controllers\Site
 */
class BaseSiteController extends Controller
{

    /**
     * BaseSiteController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
