<?php


namespace App\Http\Controllers\Test;

use App\Http\Models\Site\Site;
use App\Http\Models\Test\Test;
use App\Http\Repositories\SiteRepository;
use App\Http\Repositories\TestRepository;
use App\Http\Request\SiteRequest;
use App\Http\Services\GoogleApi\GoogleTool;
use App\Jobs\RunTestSite;
use http\Client\Request;
use Illuminate\Foundation\Http\FormRequest;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/**
 * Class TestController
 * @package App\Http\Controllers\Test
 */
class TestController extends BaseTestController
{

    /**
     * TestController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SiteRepository $siteRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(SiteRepository $siteRepository)
    {
        $site_id = session()->get('site_id');
        $s_test  = session()->get('start_test');

        if(empty($site_id))
            return Redirect::route('site')->withErrors(['Некорретный формат теста']);

        $site = $siteRepository->getById($site_id);
        if(empty($site))
            return Redirect::route('site')->withErrors(['Произошла ошибка при старте теста']);

        if(empty($s_test))
            return Redirect::route('testInit')->with(['site_id' => $site_id]);

        $this->_assign('site_id', $site_id);
        $this->_assign('url', $site->url);
        $this->_assign('inputs', Test::Inputs());
        $this->_assign('outputs', Test::Outputs());

        return view('test.single_test');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function init(TestRepository $testRepository)
    {
        $site_id = session()->get('site_id');
        $test    = $testRepository->getBySiteId($site_id);

        if(!empty($site_id) && $test->isEmpty())
            dispatch(new RunTestSite($site_id));

        return Redirect::route('test')->with(['site_id' => $site_id, 'start_test' => true]);
    }
}
