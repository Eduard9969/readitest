<?php


namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;

/**
 * Class BaseTestController
 * @package App\Http\Controllers\Test
 */
class BaseTestController extends Controller
{

    /**
     * BaseTestController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
