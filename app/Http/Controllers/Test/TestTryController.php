<?php


namespace App\Http\Controllers\Test;

use App\Http\Models\Test\Test;
use App\Http\Repositories\SiteRepository;
use App\Http\Repositories\TestRepository;
use App\Http\Services\GoogleApi\GoogleTool;
use App\Http\Services\GtmetrixApi\GtmetrixTool;
use App\Http\Services\Math\LogicScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class TestTryController
 * @package App\Http\Controllers\Test
 */
class TestTryController extends BaseTestController
{

    private $tGtmetrix;

    /**
     * TestTryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->tGtmetrix = new GtmetrixTool();
    }

    public function testing(Request $request, SiteRepository $siteRepository, TestRepository $testRepository) {
        set_time_limit(0);

        $site_id    = $request->post('site_id');
        $secret_key = $request->post('secret_code');

        Log::info('Get Site Id ' . $site_id);
        Log::info('Get Secret Key ' . $secret_key);

        if(empty($site_id) || empty($secret_key) || $secret_key != config('api.app_secret_key'))
        {
            die();
        }

        $site = $siteRepository->getById($site_id);
        Log::info('Site ' . $site->url);
        if (empty($site)) die();

        $url = $site->url;

        $tGtmetrix = $this->tGtmetrix;
        $test = $tGtmetrix->testRun($url);
        if ($tGtmetrix->hasError()) die();

        $gtmetrix_data = $this->retestGtmetrix($test->getId());
        Log::info('GtMetrixData From Api => ' . \GuzzleHttp\json_encode($gtmetrix_data));
//        $gtmetrix_data = $this->reTestGtmetrix('qOQdBPyt');

        $array_2 = [];
        $count_true_params = 0;
        foreach ($gtmetrix_data as $key => $item)
        {
            if(in_array($key, [
                'htmlBytes',
                'htmlLoadTime',
                'pageBytes',
                'pageElements',
                'redirectDuration',
                'rumSpeedIndex',
                ''])
            ) continue;

            if (in_array($key, ['pagespeedScore', 'yslowScore', ''])) {
                $array_2[] = $item >= 60;
                $count_true_params++;
            }
            elseif(!in_array($key, ['test_id', 'reportUrl']))
            {
                if(in_array($key, ['connectDuration', 'backendDuration', 'onloadDuration']))
                {
                    $array_2[] = $item <= 75;
                    $count_true_params++;
                } else
                {
                    $array_2[] = $item <= 1000;
                    $count_true_params++;
                }
            }
        }
        $accuracy = $count_true_params > 0 ? ($count_true_params * 100 / count($array_2)) : 0;


        $tGoogle = new GoogleTool();
        $test = $tGoogle->testRun($url);
        Log::info('Google Errors From Api => ' . \GuzzleHttp\json_encode($tGoogle->getErrors()));
        if ($tGoogle->hasError()) die();

        $data = $test->getLighthouseResult()->getAudits();
        Log::info('Google Data From Api => ' . \GuzzleHttp\json_encode($data));

        $count_params = $count_true_params = 0;

        $array_1 = [];
        foreach ($data as $key => $item)
        {
//            $result = $item->getDetails();
            $score  = $item->getScore();

            $count_params++;
            if(!empty($score))
                $count_true_params++;

            $array_1[] = !empty($score);
//            if (isset($result['items']) && !empty($result['items']))
//            {
//                $google_data[$key] = $result['items'];
//            }
        }

        $suitability = round(100 * $count_true_params / $count_params, 1);

        $sLogicScore = new LogicScore();
        $sLogicScore->setParams(1, $array_1);
        $sLogicScore->setParams(2, $array_2);
        $sLogicScore->setParams(3, [false]);
        $sLogicScore->setParams(4, [false]);
        $sLogicScore->setParams(5, [true, false, true]);

        $inputs = Test::Inputs();

        $attributes = ['site_id' => $site_id];
        foreach ($inputs as $key => $input)
        {
            if(in_array($key, ['compliance', 'interoperability']))
            {
                $attributes[$key] = 0;
            }
            elseif($key == 'suitability')
            {
                $attributes[$key] = $suitability;
            }
            elseif ($key == 'accuracy')
            {
                $attributes[$key] = $accuracy;
            }
            else
            {
                $attributes[$key] = round(2 * 100 / 3);
            }
        }

        $attributes['confidence']       = $sLogicScore->getOutput();
        $attributes['confidence_title'] = $sLogicScore->getOutputName();

        Log::info('Final Attr Stories ' . \GuzzleHttp\json_encode($attributes));
        $testRepository->create($attributes);
    }

    /**
     * ReTest Gtmetrix Result
     * @param $test_id
     * @return array
     */
    private function reTestGtmetrix($test_id)
    {
        $response = $this->tGtmetrix->checkTestStatus($test_id);

        $data = [];
        if($response->getState() != 'completed')
        {
            sleep(1);
            return $this->reTestGtmetrix($test_id);
        }

        $data = [
            'test_id' => $test_id,
            'reportUrl' => $response->getReportUrl(),
            'pagespeedScore' => $response->getPagespeedScore(),
            'yslowScore' => $response->getYslowScore(),
            'htmlBytes' => $response->getHtmlBytes(),
            'htmlLoadTime' => $response->getHtmlLoadTime(),
            'pageBytes' => $response->getPageBytes(),
            'pageLoadTime' => $response->getPageLoadTime(),
            'pageElements' => $response->getPageElements(),
            'redirectDuration' => $response->getRedirectDuration(),
            'connectDuration' => $response->getConnectDuration(),
            'backendDuration' => $response->getBackendDuration(),
            'firstPaintTime' => $response->getFirstPaintTime(),
            'domInteractiveTime' => $response->getDomInteractiveTime(),
            'domContentLoadedTime' => $response->getDomContentLoadedTime(),
            'domContentLoadedDuration' => $response->getDomContentLoadedDuration(),
            'onloadTime' => $response->getOnloadTime(),
            'onloadDuration' => $response->getOnloadDuration(),
            'fullyLoadedTime' => $response->getFullyLoadedTime(),
            'rumSpeedIndex' => $response->getRumSpeedIndex()
        ];

        return $data;
    }
}
