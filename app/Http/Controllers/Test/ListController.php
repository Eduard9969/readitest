<?php


namespace App\Http\Controllers\Test;

use App\Http\Repositories\TestRepository;

/**
 * Class ListController
 * @package App\Http\Controllers\Test
 */
class ListController extends BaseTestController
{

    public function list(TestRepository $testRepository)
    {
        $this->_assign('tests', $testRepository->getAll());
        $this->_assign('last_list', true);

        return view('test.list_test');
    }

}
