<?php


namespace App\Http\Controllers\Test\AjaxTest;

use App\Http\Controllers\ControllerAjax;

/**
 * Class BaseAjaxTestController
 * @package App\Http\Controllers\Test\AjaxTest
 */
class BaseAjaxTestController extends ControllerAjax
{

    /**
     * BaseAjaxTestController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
