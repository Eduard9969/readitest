<?php


namespace App\Http\Controllers\Test\AjaxTest;


use App\Http\Controllers\ControllerAjax;
use App\Http\Models\Test\Test;
use App\Http\Repositories\TestRepository;

/**
 * Class CheckTestStatusController
 * @package App\Http\Controllers\Test\AjaxTest
 */
class CheckTestStatusController extends BaseAjaxTestController
{
    /**
     * CheckTestStatusController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param TestRepository $testRepository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkstatus(TestRepository $testRepository, $id)
    {
        $test = $testRepository->getBySiteId($id);

        $result  = [];
        $test_id = 0;

        if(!$test->isEmpty())
        {
            $test = $test->toArray();

            $test_id = $test[0]->id ?? 0;

            $inputs  = Test::Inputs();
            $outputs = Test::Outputs();

            foreach ($test[0] as $key => $input)
            {
                if(key_exists($key, $inputs) || key_exists($key, $outputs))
                    $result[$key] = $input;
            }

        }

        return response()->json([
            'status'   => (int) !empty($result),
            'test_id'  => $test_id,
            'result'   => $result,
            'is_final' => !empty($result)
        ]);
    }

}
