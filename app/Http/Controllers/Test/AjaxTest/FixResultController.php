<?php


namespace App\Http\Controllers\Test\AjaxTest;


use App\Http\Models\Test\SimilarTest;
use App\Http\Models\Test\Test;
use App\Http\Repositories\SimilarTestRepository;
use App\Http\Repositories\SiteRepository;
use App\Http\Repositories\TestRepository;

/**
 * Class FixResultController
 * @package App\Http\Controllers\Test\AjaxTest
 */
class FixResultController extends BaseAjaxTestController
{

    /**
     * @param TestRepository $testRepository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function fixresult(TestRepository $testRepository, SimilarTestRepository $similarTestRepository, $id)
    {
        $test = $testRepository->getById($id);
        if ($test->isEmpty())
            return response()->json(['message' => 'Данный тест не актуален']);

        $id = $similarTestRepository->create(['test_id' => $id]);
        if (empty($id))
            return response()->json(['message' => 'Произошла ошибка при сохранении']);

        return response()->json([
            'status'    => 1,
            'message'   => 'Успешно добавлено в сравнительную таблицу'
        ]);
    }

    public function addnewdata(TestRepository $testRepository, $id)
    {
        $addons = request()->post('adddata');

        $test = $testRepository->getById($id);
        if ($test->isEmpty() || empty($addons) || !is_array($addons))
        {
            return response()->json(['message' => 'Произошла ошибка']);
        }

        $inputs = Test::Inputs();
        $params = [];

        /**
         * Обновления новых параметров
         */
        foreach ($addons as $key => $item)
        {
            if (key_exists($key, $inputs))
            {
                $all    = $item['all'] ?? 0;
                $true   = $item['true'] ?? 0;

                if (empty($all)) continue;
                if ($true > $all) $true = $all;

                $params[$key] = empty($true) ? 0 : round($true / $all * 100, 1);
            }
        }

        /**
         * Дополнения текущими параметрами
         */
        foreach ($test[0] as $key => $value)
        {
            if (key_exists($key, $inputs) && !key_exists($key, $params))
            {
                $params[$key] = $value;
            }
        }

        $site_id = $test[0]->site_id ?? 0;
        $outputs  = Test::Outputs();

        $output = '';
        foreach ($outputs as $key => $value)
        {
            $output = $key;
            break;
        }

        if (!empty($output))
            $params = $this->remathUpdateOutputData($output, $params);

        if (empty($params))
        {
            return response()->json(['message' => 'Произошла ошибка']);
        }

        $testRepository->UpdateByid($id, $params);
        return response()->json([
            'status'     => 1,
            'message'    => 'Данные обновлены',
            'site_id'    => $site_id
        ]);
    }

    /**
     * Пересчет новых значений
     *
     * @param $output_key
     * @param $inputs
     * @return mixed
     */
    private function remathUpdateOutputData($output_key, $inputs)
    {
        $inputs_list  = Test::Inputs();

        $inputs_count = count($inputs_list);
        $part_percent = 100/$inputs_count;

        $output_value = 0;
        foreach ($inputs as $input)
        {
            $output_value += $input > 0 ? ($part_percent / 100 * $input) : 0;
        }

        $params = $inputs;
        $params[$output_key] = round($output_value, 1);

        return $params;
    }
}
