<?php


namespace App\Http\Controllers\Test;

use App\Http\Models\Test\Test;
use App\Http\Repositories\TestRepository;

class ExpertsController extends BaseTestController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param TestRepository $testRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function experts(TestRepository $testRepository)
    {

        $this->_assign('tests',   $testRepository->getAllExperts());
        $this->_assign('inputs',  Test::Inputs());
        $this->_assign('outputs', Test::Outputs());

        return view('test.list_test');
    }

}
