<?php


namespace App\Http\Controllers;

use App\Http\Models\Test\Test;
use App\Http\Repositories\TestRepository;

/**
 * Class ControllerAjax
 * @package App\Http\Controllers
 */
class ControllerAjax extends Controller
{

    /**
     * ControllerAjax constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
