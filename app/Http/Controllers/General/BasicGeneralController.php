<?php


namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;

/**
 * Class BasicGeneral
 * @package App\Http\Controllers\General
 */
class BasicGeneralController extends Controller
{

    /**
     * BasicGeneral constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
