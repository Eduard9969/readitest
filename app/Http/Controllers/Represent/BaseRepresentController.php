<?php


namespace App\Http\Controllers\Represent;
use App\Http\Controllers\Controller;

/**
 * Class RepresentController
 * @package App\Http\Controllers\Represent
 */
class BaseRepresentController extends Controller
{

    /**
     * RepresentController constructor.
     */
    function __construct() {
        parent::__construct();
    }

}
