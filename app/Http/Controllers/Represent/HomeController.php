<?php


namespace App\Http\Controllers\Represent;

/**
 * Class HomeController
 * @package App\Http\Controllers\Prevent
 */
class HomeController extends BaseRepresentController
{

    /**
     * Страница презентация
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function home()
    {
        return view('represent.home', [
            'title'        => 'Оценка качества разрабатываемого продукта',
            'second_title' => 'Автоматизированная система'
        ]);
    }

}
