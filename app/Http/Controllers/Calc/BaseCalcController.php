<?php


namespace App\Http\Controllers\Calc;

use App\Http\Controllers\Controller;

/**
 * Class BaseCalcController
 * @package App\Http\Controllers\Calc
 */
class BaseCalcController extends Controller
{

    /**
     * BaseCalcController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

}
