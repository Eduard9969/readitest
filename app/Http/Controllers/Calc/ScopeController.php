<?php


namespace App\Http\Controllers\Calc;

use App\Http\Services\Math\LogicScore;

/**
 * Class ScopeController
 * @package App\Http\Controllers\Calc
 */
class ScopeController extends BaseCalcController
{

    public function scopes()
    {
        $mLogic = new LogicScore();

        for ($i = 0; $i < 5; $i++)
        {
            $params = [];
            for ($y = 0; $y < rand(1, 10); $y++)
            {
                $params[$y] = (bool) rand(0, 1);
            }
            $mLogic->setParams($i, $params);
        }

        $output = $mLogic->getOutput();
        if($mLogic->hasError())
        {
            echo $mLogic->getErrors()[0];
            exit();
        }
    }

}
