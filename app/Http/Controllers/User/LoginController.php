<?php

namespace App\Http\Controllers\User;

use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class LoginController extends BaseUserController
{

    public function __construct()
    {
        parent::__construct();

        self::setViewAssign('title', 'Авторизация в системе');
    }

    /**
     * Страница авторизации
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    function signin()
    {
        return view('user.login');
    }
}
