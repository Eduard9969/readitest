<?php

namespace App\Http\Controllers\User;

use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class RegisterController
 * @package App\Http\Controllers\User
 */
class RegisterController extends BaseUserController
{

    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        self::setViewAssign('title', 'Регистрация в системе');
    }

    /**
     * Регистрация пользователя
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
   public function register()
   {
       return view('user.register');
   }

    /**
     * Восстановления пароля
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
   public function forgot()
   {
       return view('user.forgot');
   }
}
