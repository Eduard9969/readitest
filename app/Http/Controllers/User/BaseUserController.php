<?php


namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

/**
 * Class BaseUserController
 * @package App\Http\Controllers\User
 */
class BaseUserController extends Controller
{

    /**
     * BaseUserController constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

}
