<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Repositories\LoginRepository;
use App\Http\Request\AuthLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    /**
     * Авторизация
     *
     * @param AuthLoginRequest $request
     * @param LoginRepository $loginRepository
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function auth(AuthLoginRequest $request, LoginRepository $loginRepository)
    {
        $login = !is_null($request->login)    ? $request->login    : '';
        $email = !is_null($request->email)    ? $request->email    : '';
        $pass  = !is_null($request->password) ? $request->password : '';

        $user  = !empty($login) ? $loginRepository->getUserByLogin($login)
                                    : $loginRepository->getUserByEmail($email);

        $message = '';
        /*
         * Если пользователь не найден, заполняем $message
         */
        if (empty($user))
        {
            $message = 'Пользователь с такими данными не найден';
        }

        /*
         * Если нет ошибки раннее, авторизуем
         */
        if(empty($message))
        {
            $login = $login ?? isset($user['login']) ? $user['login'] : null;

            $authData = [
                'login'    => $login,
                'password' => $pass
            ];

            /*
             * Если ошибка авторизации, заполняем $message
             */
            if(Auth::attempt($authData))
            {
                return Redirect::route('dashboard');
            }
            else
            {
                $message = 'Произошла ошибка при попытке входа';
            }
        }

        return Redirect::back()->withInput()->withErrors([$message]);
    }
}
