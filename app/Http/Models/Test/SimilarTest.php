<?php


namespace App\Http\Models\Test;


use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SimilarTest extends Model
{
    protected $model;

    public function __construct(array $attributes = [])
    {
        $this->model = DB::table($this->getTable());
        parent::__construct($attributes);
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        if(!isset($attributes['created_at']))
            $attributes['created_at'] = Carbon::now()->toDateTimeString();
        if(!isset($attributes['updated_at']))
            $attributes['updated_at'] = Carbon::now()->toDateTimeString();

        return $this->model->insertOrIgnore($attributes);
    }
}
