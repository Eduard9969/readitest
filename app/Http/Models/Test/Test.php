<?php


namespace App\Http\Models\Test;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Test
 * @package App\Http\Models\Test
 */
class Test extends Model
{

    /**
     * @var \Illuminate\Database\Query\Builder
     */
    private $model;

    /**
     * Test constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->model = DB::table('tests');
        parent::__construct($attributes);
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function create($attributes)
    {
        if(!isset($attributes['created_at']))
            $attributes['created_at'] = Carbon::now()->toDateTimeString();
        if(!isset($attributes['updated_at']))
            $attributes['updated_at'] = Carbon::now()->toDateTimeString();

        return $this->model->insertGetId($attributes);
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->model->select()->join('sites', 'sites.id', '=', 'tests.site_id')->get()->toArray();
    }

    public function getListExperts()
    {
        return $this->model->select()
                           ->join('sites', 'sites.id', '=', 'tests.site_id')
                           ->join('similar_tests', 'tests.id', '=', 'similar_tests.test_id')
                           ->get()->toArray();
    }

    /**
     * @param $site_id
     * @return \Illuminate\Support\Collection
     */
    public function getBySiteId($site_id)
    {
        return $this->model->select()->where(['site_id' => $site_id])->limit(1)->get();
    }

    /**
     * @param $test_id
     * @return \Illuminate\Support\Collection
     */
    public function getById($test_id)
    {
        return $this->model->select()->where(['id' => $test_id])->limit(1)->get();
    }

    /**
     * @param $test_id
     * @param $params
     * @return int
     */
    public function UpdateByid($test_id, $params)
    {
        return $this->model->where(['id' => $test_id])->update($params);
    }

    /**
     * Inputs Data
     * @return array
     */
    public static function Inputs()
    {
        return [
            'suitability'      => ['title' => 'Функцианальная пригодность'],
            'accuracy'         => ['title' => 'Точность'],
            'interoperability' => ['title' => 'Способность к взаимодействию'],
            'compliance'       => ['title' => 'Соответствие'],
            'security'         => ['title' => 'Защищенность '],
        ];
    }

    /**
     * Outputs Data
     * @return array
     */
    public static function Outputs()
    {
        return [
            'confidence' => ['title' => 'Степень уверенности']
        ];
    }
}
