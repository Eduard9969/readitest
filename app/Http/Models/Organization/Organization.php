<?php


namespace App\Http\Models\Organization;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Organization
 * @package App\Http\Models\Organization
 */
class Organization extends Model
{

    /**
     * Organization constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
