<?php


namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon as Carbon;

/**
 * Class Site
 * @package App\Http\Models\Site
 */
class Site extends Model
{

    /**
     * @var \Illuminate\Database\Query\Builder
     */
    protected $model;


    /**
     * Site constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->model = DB::table($this->getTable());
        parent::__construct($attributes);
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        if(!isset($attributes['created_at']))
            $attributes['created_at'] = Carbon::now()->toDateTimeString();
        if(!isset($attributes['updated_at']))
            $attributes['updated_at'] = Carbon::now()->toDateTimeString();

        return $this->model->insertGetId($attributes);
    }

    /**
     * @return Site[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getList()
    {
        return Site::all()->all();
    }

    /**
     * @param $url
     * @return \Illuminate\Support\Collection
     */
    public function getByUrl($url)
    {
        return $this->model->select()->where(['url' => $url])->limit(1)->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }
}
