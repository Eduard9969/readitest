<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('site_id');
            $table->double('suitability', 8, 2);
            $table->double('accuracy', 8, 2);
            $table->double('interoperability', 8, 2);
            $table->double('compliance', 8, 2);
            $table->double('security', 8, 2);
            $table->double('confidence', 8, 2);
            $table->string('confidence_title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
