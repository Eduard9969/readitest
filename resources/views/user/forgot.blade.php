@extends('layout.main')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <!-- brand -->
                @include('layout.detail.logo')
                <!-- / brand -->
            </div>
        </div>
    </div>
    <div class="b-t">
        <div class="center-block w-xxl w-auto-xs p-y-md text-center">
            <div class="p-a-md">
                <div>
                    <h4>Забыли свой пароль?</h4>
                    <p class="text-muted m-y">
                        Введите свой email и вы получите дальнейший инструкции по восстановлению.
                    </p>
                </div>

                <form name="reset">
                    <div class="form-group">
                        <input type="email" placeholder="Email" class="form-control" required>
                    </div>
                    <button type="submit" class="btn black btn-block p-x-md" >Отправить</button>
                </form>

                <div class="p-y-lg">
                    Вернуться в
                    <a href="{{ route('signIn') }}" class="text-primary _600">Войти</a>
                </div>
            </div>
        </div>
    </div>
@endsection
