@extends('layout.main')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <!-- brand -->
                @include('layout/detail/logo')
                <!-- / brand -->
            </div>
        </div>
    </div>
    <div class="b-t">
        <div class="center-block w-auto-xs p-y-md text-center">
            <div class="p-a-md container">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <form name="form" action="{{ route('loginControl') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email или Логин" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Пароль" required>
                            </div>
                            <div class="m-b-md">
                                <label class="md-check">
                                    <input type="checkbox" name="remember">
                                    <i class="primary"></i> Оставаться в системе
                                </label>
                            </div>

                            <button type="submit" class="btn btn-lg black p-x-lg">Войти</button>
                        </form>
                        <div class="m-y">
                            <a href="{{ route('forgot') }}" class="_600">Забыли пароль?</a>
                        </div>
                        <div>
                            Вы еще не зарегистрированы?
                            <a href="{{ route('signUp') }}" class="text-primary _600">Присоединиться</a>
                        </div>
                    </div>

                    <div class="col-12 col-md-2">
                        <div class="m-y text-sm">
                            ИЛИ
                        </div>
                    </div>

                    <div class="col-12 col-md-5">
                        <div>
                            <a href="#" class="btn btn-block indigo text-white m-b-sm">
                                <i class="fa fa-facebook pull-left"></i>
                                Войти через Facebook
                            </a>
                            <a href="#" class="btn btn-block red text-white">
                                <i class="fa fa-google-plus pull-left"></i>
                                Войти через Google+
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
