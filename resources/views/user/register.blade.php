@extends('layout.main')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <!-- brand -->
                @include('layout.detail.logo')
                <!-- / brand -->
            </div>
        </div>
    </div>
    <div class="b-t">
        <div class="center-block w-xxl w-auto-xs p-y-md text-center">
            <div class="p-a-md">
                <form name="form" action="home.html">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Пароль" required>
                    </div>
                    <div class="m-b-md text-left">
                        <label class="md-check">
                            <input type="checkbox" name="remember">
                            <i class="primary"></i> Являюсь <a href="#" class="text-primary font" title="Политика представителей организации">представителем</a> организации
                        </label>
                    </div>

                    <div class="m-b-md text-sm">
                        <span class="text-muted">Регистрируясь Вы соглащаетесь с условиями </span>
                        <a href="#">Terms of service</a>
                        <span class="text-muted">и</span>
                        <a href="#">Policy Privacy.</a>
                    </div>
                    <button type="submit" class="btn btn-lg black p-x-lg">Зарегистрироваться</button>
                </form>
                <div class="p-y-lg text-center">
                    <div>У Вас уже есть аккаунт? <a href="{{ route('signIn') }}" class="text-primary _600">Войти</a></div>
                </div>
            </div>
        </div>
    </div>
@endsection
