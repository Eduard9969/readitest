<a href="{{ route('RepresentHome') }}" class="navbar-brand">
    <div data-ui-include="{{ URL::asset('img/logo.svg') }}"></div>
    <img src="{{ URL::asset('img/logo.png') }}" alt="@if(isset($logo)) {{ $logo }} @endif" class="hide">
    <span class="hidden-folded inline">@if(isset($logo)) {{ $logo }} @else #none @endif</span>
</a>
