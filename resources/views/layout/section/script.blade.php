<!-- ############ SWITHCHER START-->
<div id="switcher">
    <div class="switcher dark-white" id="sw-theme">
        <a href="#" data-ui-toggle-class="active" data-ui-target="#sw-theme" class="dark-white sw-btn">
            <i class="fa fa-gear text-muted"></i>
        </a>
        <div class="box-body">
            <p>Цветовая схема:</p>
            <p data-target="color">
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="primary">
                    <i class="primary"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="accent">
                    <i class="accent"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="warn">
                    <i class="warn"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="success">
                    <i class="success"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="info">
                    <i class="info"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="warning">
                    <i class="warning"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="danger">
                    <i class="danger"></i>
                </label>
            </p>
            <p>Тема:</p>
            <div data-target="bg" class="clearfix">
                <label class="radio radio-inline m-a-0 ui-check ui-check-lg">
                    <input type="radio" name="theme" value="">
                    <i class="light"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-lg">
                    <input type="radio" name="theme" value="grey">
                    <i class="grey"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-lg">
                    <input type="radio" name="theme" value="dark">
                    <i class="dark"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-lg">
                    <input type="radio" name="theme" value="black">
                    <i class="black"></i>
                </label>
            </div>
        </div>
    </div>
</div>
<!-- ############ SWITHCHER END-->

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
<script src="{{ URL::asset('libs/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ URL::asset('libs/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ URL::asset('libs/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- core -->
<script src="{{ URL::asset('libs/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
<script src="{{ URL::asset('libs/PACE/pace.min.js') }}"></script>
<script src="{{ URL::asset('libs/jquery-pjax/jquery.pjax.js') }}"></script>
<script src="{{ URL::asset('libs/blockUI/jquery.blockUI.js') }}"></script>
<script src="{{ URL::asset('libs/jscroll/jquery.jscroll.min.js') }}"></script>

<script src="{{ URL::asset('js/config.lazyload.js') }}"></script>
<script src="{{ URL::asset('js/ui-load.js') }}"></script>
<script src="{{ URL::asset('js/ui-jp.js') }}"></script>
<script src="{{ URL::asset('js/ui-include.js') }}"></script>
<script src="{{ URL::asset('js/ui-device.js') }}"></script>
<script src="{{ URL::asset('js/ui-form.js') }}"></script>
<script src="{{ URL::asset('js/ui-modal.js') }}"></script>
<script src="{{ URL::asset('js/ui-nav.js') }}"></script>
<script src="{{ URL::asset('js/ui-list.js') }}"></script>
<script src="{{ URL::asset('js/ui-screenfull.js') }}"></script>
<script src="{{ URL::asset('js/ui-scroll-to.js') }}"></script>
<script src="{{ URL::asset('js/ui-toggle-class.js') }}"></script>
<script src="{{ URL::asset('js/ui-taburl.js') }}"></script>
<script src="{{ URL::asset('js/app.js') }}"></script>
<script src="{{ URL::asset('js/ajax.js') }}"></script>
<script src="{{ URL::asset('js/custom.js') }}"></script>
<!-- endbuild -->
