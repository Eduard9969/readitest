<!-- style -->
<link rel="stylesheet" href="{{ URL::asset('css/animate.css/animate.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/glyphicons/glyphicons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/material-design-icons/material-design-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/ionicons/css/ionicons.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />

<!-- build:css css/styles/app.min.css -->
<link rel="stylesheet" href="{{ URL::asset('css/styles/app.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/styles/style.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css" />
<!-- endbuild -->
<link rel="stylesheet" href="{{ URL::asset('css/styles/font.css') }}" type="text/css" />
