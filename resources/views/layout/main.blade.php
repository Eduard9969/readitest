<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">

        <title>@if (isset($title)){{ $title }}@endif @if (isset($second_title))- {{ $second_title }} @endif @if(isset($logo)){{ $logo }} @endif</title>

        <meta name="desctiption" content="" />
        <meta name="keywords" content="" />

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        {{-- styles --}}
        @include('layout/section/style')
    </head>

    <body>
        <div class="app" id="app">
            <div class="notification_area pos-f-t m-r-sm m-t-md">
                @if($errors->any())
                    <div class="box-color danger pos-rlt pointer" onclick="this.remove()">
                        <span class="arrow right b-danger"></span>
                        <div class="box-body p-y-1">
                            {{$errors->first()}}
                        </div>
                    </div>
                @endif

                @if ($session::has('message') && !$errors->any())
                    <div class="box-color success pos-rlt pointer" onclick="this.remove()">
                        <span class="arrow right b-success"></span>
                        <div class="box-body p-y-1">
                            {{ $session::get('message') }}
                        </div>
                    </div>
                    {{ $session::remove('message') }}
                @endif
            </div>

            @yield('content')
        </div>

        {{-- script --}}
        @include('layout/section/script')
    </body>

</html>
