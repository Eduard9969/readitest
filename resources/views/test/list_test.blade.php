@extends('layout.main')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <!-- brand -->
            @include('layout/detail/logo')
            <!-- / brand -->
            </div>
        </div>
    </div>

    <div class="center-block w-auto-xs p-y-md text-center p-t-0">
        <div class="p-a-md container">
            <div class="box text-left">
                <div class="box-header">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <h2>@if(!empty($last_list)) Список выполненных тестов @else Сравнительный анализ тестов@endif</h2>
                            <small>Список @if(!empty($last_list)) последних @else зафиксированных @endif тестов </small>
                        </div>
                        <div class="col-12 col-md-6 text-md-right">
                            @if(!empty($last_list))
                            <a href="{{ route('site') }}" class="btn btn-primary">
                                <i class="fa fa-plus"></i>
                                <span class="m-l-1">Добавить</span>
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered m-a-0">
                        @if(!empty($last_list))
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Веб - ресурс</th>
                                <th>Ссылка</th>
                                <th class="text-center">Проектная группа</th>
                                <th class="text-center">Статус тестирования</th>
                                <th class="text-center">Время инициализации</th>
                            </tr>
                            </thead>
                            @if(!empty($tests))
                                @foreach($tests as $key => $test)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $test->name }}</td>
                                        <td>{{ $test->url }}</td>
                                        <td class="text-center">{{ $test->project_id }}</td>
                                        <td class="text-center">@if(!empty($test->confidence_title)) <span class="text-success">Выполнено</span> @else <span class="text-warn">Выполняется</span>  @endif</td>
                                        <td class="text-center">{{ $test->created_at }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="6" class="text-center">
                                        <a href="{{ route('testExperts') }}" class="btn btn-sm btn-warning">
                                            Сравнительный анализ тестов
                                        </a>
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td colspan="7" class="center">
                                        Пока не было запущено ни одного теста
                                    </td>
                                </tr>
                            @endif
                        @else
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; font-size: 80%">ID теста</th>
                                    <th style="vertical-align: middle; font-size: 80%">Веб - ресурс</th>
                                    @foreach($inputs as $input)
                                        <th style="vertical-align: middle; text-align: center; font-size: 80%">{{ $input['title'] }}</th>
                                    @endforeach
                                    @foreach($outputs as $output)
                                        <th style="vertical-align: middle; text-align: center; font-size: 80%">{{ $output['title'] }}</th>
                                    @endforeach
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($tests))
                                    @foreach($tests as $test)
                                        <tr>
                                            <td style="vertical-align: middle">
                                                {{ $test->id }}
                                            </td>
                                            <td style="vertical-align: middle">
                                                {{ $test->name }}
                                                <small class="text-sm d-block">{{ $test->url }}</small>
                                            </td>
                                            @foreach($inputs as $key => $input)
                                                <td style="vertical-align: middle; text-align: center">{{ $test->$key }} %</td>
                                            @endforeach
                                            @foreach($outputs as $key => $output)
                                                <td style="vertical-align: middle; text-align: center">{{ $test->$key }} %</td>
                                            @endforeach
                                            <td style="vertical-align: middle; text-align: center">
                                                <a href="/delete" title="Удалить сравнения">
                                                    <i class="fa fa-ban"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="center">
                                            Пока не было запущено ни одного теста
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>

            @if(empty($last_list))
                <div class="row">
                    @foreach($tests as $k => $test)
                        <div class="col-sm-6">
                            <div class="box">
                                <div class="box-header">
                                    <h3>{{ $test->name }}</h3>
                                    <small class="block text-muted">{{ $test->url }}</small>
                                </div>
                                <div class="box-body">
                                    <div data-ui-jp="echarts" data-ui-options="{
                                    @foreach($outputs as $key => $output)
                                    title: {
                                        text: '{{ $test->$key }}%',
                                        subtext: '{{ $output['title'] }}',
                                        x: 'center',
                                        y: 'center',
                                        itemGap: 20,
                                        textStyle : {
                                            color : 'rgba(210,105,30,1)',
                                            fontSize : 20,
                                            fontWeight : 'bolder'
                                        }
                                    },
                                    @endforeach
                                    tooltip : {
                                      trigger: 'item',
                                      formatter: '{b}<br/> ({d}%)'
                                    },
                                    calculable : true,
                                    series : [
                                        {
                                            name:'График распределения',
                                            type:'pie',
                                            radius : ['50%', '70%'],
                                            data:[
                                                @foreach($inputs as $key => $input)
                                                    {value: ((100 / {{ count($inputs) }}) * {{ $test->$key }} / 100), name: '{{ $input['title'] }}'},
                                                @endforeach
                                                @foreach($outputs as $key => $output)
                                                    {value: {{ 100 - $test->$key }}, name: 'Не пройдено'},
                                                @endforeach
                                            ]
                                        }
                                    ]
                                }" style="height:300px" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="box">
                            <div class="box-header">
                                <h3>Пузырьковая диаграмма</h3>
                                <small class="block text-muted">Диаграма представления сравнительного анализа</small>
                            </div>
                            <div class="box-body">
                                <canvas data-ui-jp="chart" data-ui-options="
                                  {
                                    type: 'bubble',
                                    data: {
                                        datasets: [
                                          { data: [{x:0, y:0, r:0}], label: 'Базовая точка' },
                                          @foreach($tests as $k => $test)
                                          {
                                              data: [{
                                                x: {{ $k + 1 }},
                                                y: @foreach($outputs as $key => $output) {{ (float) $test->$key }} @endforeach,
                                                r: {{ rand(5, 12) }}
                                              }],
                                              backgroundColor: '{{ sprintf('#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255)) }}',
                                              label: '{{ $test->name }}'
                                          },
                                          @endforeach
                                        ]
                                    },
                                    options: {}
                                  }
                                  " height="240">
                                </canvas>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
