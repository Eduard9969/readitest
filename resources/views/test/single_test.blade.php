@extends('site.page_create')

@section('test_content')
    <input type="hidden" name="runtest" value="@if(isset($site_id)){{ $site_id }}@endif">

    <div class="center-block w-auto-xs p-y-md text-center p-t-0">
        <div class="text-preview-test">
            <span>Выполняется тестирование...<br></span>
            <span class="text-muted">Нет конфетки - Не покажу результаты</span>
        </div>

        <div class="p-a-md container" data-action="testing">
            <div class="row no-gutter b-l b-r">
                <div class="col-sm-1"></div>
                @foreach($inputs as $key => $input)
                    <div class="col-xs-6 col-sm-2 b-r b-b @if ($key == 'suitability') b-l  @endif">
                        <div class="padding">
                            <div class="text-center">
                                <h2 class="text-center _600" data-input-value="{{ $key }}">
                                    <span class="text-blink">-</span>
                                </h2>
                                <p class="text-muted m-b-md" style="min-height: 42px">{{ $input['title'] }}</p>
                                <div>
                                    <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"><canvas width="60" height="20" style="display: inline-block; width: 60px; height: 20px; vertical-align: top;"></canvas></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                @foreach($outputs as $key => $output)
                    <div class="col-xs-12 b-b">
                        <div class="padding">
                            <div class="text-center">
                                <h2 class="text-center _600" data-input-value="{{ $key }}">
                                    <span class="text-blink">-</span>
                                </h2>
                                <p class="text-muted m-b-md" style="min-height: 42px">{{ $output['title'] }}</p>
                                <div>
                                    <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"><canvas width="60" height="20" style="display: inline-block; width: 60px; height: 20px; vertical-align: top;"></canvas></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="p-a-md container" data-action="post-testing" style="display: none">
            <form method="POST" id="update-test-data">
                <div class="row no-gutter b-l b-r">
                    <div class="col-12 p-b-2">
                        <h5 class="font-weight-bold">
                            Дополнение ручными данными
                        </h5>
                    </div>
                    <div class="col-12 col-md-1"></div>
                    @foreach($inputs as $key => $input)
                        @if (!in_array($key, ['interoperability', 'compliance']))
                            @continue
                        @endif
                        <div class="col-12 col-md-5 b-r b-b @if ($key == 'suitability') b-l  @endif">
                            <div class="padding">
                                <div class="text-center">
                                    <h2 class="text-center _600" style="max-width: 180px; margin: 0 auto">
                                        <div class="row">
                                            <div class="col-md-5 md-form-group p-y-0 small" style="vertical-align: middle">
                                                <label>
                                                    <input class="md-input text-center quantity" required="" name="adddata[{{$key}}][true]">
                                                    <small class="text-sm text-muted">Одобрено</small>
                                                </label>
                                            </div>
                                            <div class="col-md-2 text-muted small" style="vertical-align: middle">/</div>
                                            <div class="col-md-5 md-form-group p-y-0 small" style="vertical-align: middle">
                                                <label>
                                                    <input class="md-input text-center quantity" required="" name="adddata[{{$key}}][all]">
                                                    <small class="text-sm text-muted">Всего</small>
                                                </label>
                                            </div>
                                        </div>
                                    </h2>
                                    <p class="text-muted m-b-md" style="min-height: 42px">{{ $input['title'] }}</p>
                                    <div>
                                        <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"><canvas width="60" height="20" style="display: inline-block; width: 60px; height: 20px; vertical-align: top;"></canvas></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 col-md-1"></div>
                </div>

                <div class="row no-gutter b-l b-r b-b p-a-md">
                    <div class="col-12">
                        <button class="btn btn-success" type="submit">Дополнить</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="p-a-md p-y-sm container">
            <div class="row text-preview-btn" style="display: none">
                <div class="col-12 text-md-right">
                    <button class="btn btn-link btn-sm text-muted t-d-none" onclick="copylink();">Получить ссылку</button>
                    <button class="btn btn-dark btn-sm grey" onclick="refreshtest();">
                        <i class="fa fa-refresh m-r-1"></i>Сбросить кеш
                    </button>
                    <div class="d-inline-block" style="position: relative">
                        <button class="btn btn-primary btn-sm blue-700" onclick="fixtestresult();">Добавить в сравнение</button>
                        <span class="label label-lg dark pos-rlt m-r-xs text-sm" style="display: none; position: absolute; top: 110%; left: 0; right: 0; margin: 0 auto">
                            <b class="arrow top b-dark pull-in"></b>
                            <a href="{{route('testExperts')}}" title="" class="btn-comp">
                                Перейти к сравнению
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
