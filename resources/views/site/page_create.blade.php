@extends('layout.main')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <!-- brand -->
            @include('layout/detail/logo')
            <!-- / brand -->
            </div>
        </div>
    </div>
    <div class="b-t">
        <div class="center-block w-auto-xs p-y-md text-center">
            <div class="p-a-md container">
                <div class="row">
                    <form action="{{ route('siteCreate') }}" method="POST">
                        <div class="col-12 col-md-9">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <input type="text" name="url" class="form-control" placeholder="Введите Url Сайта" style="min-height: 41px" value="@if(isset($url)){{$url}}@else{{ old('url') }}@endif" required>
                                </div>
                        </div>

                        <div class="col-12 col-md-3">
                            <div>
                                <button type="submit" class="btn btn-block indigo text-white m-b-sm">
                                    <i class="fa fa-check-circle pull-left" style="margin-top: -1px"></i>
                                    Начать
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @yield('test_content')
@endsection
