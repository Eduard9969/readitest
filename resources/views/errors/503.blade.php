@extends('layout.main')

@section('content')
    <div class="p-a black">
        <div class="navbar" data-pjax>
            <div class="pull-center">
                <!-- brand -->
            @include('layout.detail.logo')
            <!-- / brand -->
            </div>
        </div>
    </div>

    <div id="content" class="app-content" role="main">
        <div class="app-body">
            <div class="row-col amber h-v">
                <div class="row-cell v-m">
                    <div class="text-center col-sm-6 offset-sm-3 p-y-lg">
                        <h1 class="display-3 m-y-lg">Хьюстон, это у нас проблемы</h1>
                        <p class="m-y text-muted h4">
                            -- 503 --
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
