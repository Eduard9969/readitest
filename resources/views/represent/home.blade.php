@extends('layout/main')

@section('content')
    <div class="p-a black">
        <div class="navbar" data-pjax>
            <a data-toggle="collapse" data-target="#navbar" class="navbar-item pull-right hidden-md-up m-a-0 m-l">
                <i class="ion-android-menu"></i>
            </a>
            <!-- brand -->
            @include('layout/detail/logo')
            <!-- / brand -->

            <!-- navbar collapse -->
            <div class="collapse navbar-toggleable-sm pull-right pull-none-xs" id="navbar">
                <!-- link and dropdown -->
                <ul class="nav navbar-nav text-info-hover" data-ui-nav>
                    <li class="nav-item">
                        <a href="#features" data-ui-scroll-to class="nav-link">
                            <span class="nav-text">Возможности</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#demos" data-ui-scroll-to class="nav-link">
                            <span class="nav-text">Демо</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('signUp') }}" class="nav-link">
                            <span class="nav-text text-info">Присоединиться</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('signIn') }}" class="nav-link">
                            <span class="btn btn-md rounded info">Войти</span>
                        </a>
                    </li>
                </ul>
                <!-- / link and dropdown -->
            </div>
            <!-- / navbar collapse -->
        </div>
    </div>

    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-body">

            <!-- ############ PAGE START-->

            <div class="row-col black">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="p-t-lg">
                        <h1 class="m-y-md text-white">
                            @if(isset($title))
                                {{ $title }}
                            @endif
                        </h1>
                        <h6 class="text-muted m-b-lg">
                            @if(isset($second_title))
                                {{ $second_title }}
                            @endif
                            @if(isset($logo))
                                {{ $logo }}
                            @endif
                        </h6>
                        <a href="{{ route('dashboard') }}" class="btn btn-lg rounded success p-x-md m-x">Тестировать</a>
                        <a href="{{ route('signUp') }}" class="btn btn-lg rounded p-x-md m-x">Присоединиться</a>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div class="black">
                <div>
                    <canvas data-ui-jp="chart" data-ui-options="
    {
      type: 'line',
      data: {
          labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
          datasets: [
              {
                  label: 'Dataset',
                  data: [35, 30, 28, 32, 25, 22, 19, 30, 35, 41, 35, 40, 42, 45, 43, 50, 52, 55, 65, 60, 62, 60, 70, 77, 85, 100],
                  fill: true,
                  lineTension: 0.3,
                  backgroundColor: '#2196f3',
                  borderColor: '#41abff',
                  borderWidth: 3,
                  pointBorderColor: '#2196f3',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 2,
                  pointHoverRadius: 4,
                  pointHoverBackgroundColor: '#fff',
                  pointHoverBorderColor: '#fff',
                  pointHoverBorderWidth: 2,
                  pointRadius: 0,
                  pointHitRadius: 10,
                  spanGaps: false,
                  barPercentage: 1
              }
          ]
      },
      options: {
      		maintainAspectRatio: false,
	      	scales:{
		      	xAxes: [{
		           display: false,
		           barPercentage: 0.5
		        }],
		        yAxes: [{
		           display: false
		        }]
	       	},
	       	legend:{
	       		display: false
	   		},
	   		tooltips:{
	   			enabled: false
	   		}
      }
    }
    " height="280">
                    </canvas>
                </div>
            </div>
            <div class="row-col info h-v p-t-lg">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <div class="p-a-lg">
                        <h3 class="m-y-lg">Один шаг к тестированию</h3>
                        <p class="text-muted m-b-lg text-md">
                            Ваши проекты доступны на одном главном экране. <br>
                            Вы найдете необходимые инструменты в одно нажатие.
                        </p>
                        <a href="{{ route('signIn') }}" class="btn rounded btn-outline b-white b-2x m-b-lg p-x-md">Начать Сейчас</a>
                    </div>
                </div>
                <div class="col-sm-6" style="min-height: 320px; background-image:url({{ URL::asset('img/demos/dashboard.png') }}); background-size: cover">
                </div>
            </div>
            <div class="p-y-lg b-b box-shadow-z0 dark-white" id="features">
                <div class="container p-y-md">
                    <h4 class="text-center m-b-lg">Комплексное тестирование</h4>
                    <div class="row">
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-ios-monitor-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">100% Responsive</h6>
                            <p class="text-muted">Developed to be mobile first, we use a handful of media queries to create sensible breakpoints for our layouts and interfaces. </p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-ios-paper-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Application Layout</h6>
                            <p class="text-muted">We provide a unique layout for your application. It's easy to structure  app information. it's real application layout.</p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-ios-pie-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Data Visulization</h6>
                            <p class="text-muted">Building beautiful chart is easy. Visualize your data in different ways with animated and customisable. Great rendering performance across all modern browsers. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted text-2x">
					BS4
				</span>
                            <h6 class="_600 m-y">Bootstrap 4 framework</h6>
                            <p class="text-muted">Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.</p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-social-html5-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Ajax Powered</h6>
                            <p class="text-muted">Using Html5 pushState without reloading your page's layout or any resources (JS, CSS), giving the appearance of a fast, full page load.</p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-social-javascript-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Lazy load modules</h6>
                            <p class="text-muted">Load modules &amp; components on demand (lazy load) with jQuery. </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-social-sass text-3x"></i>
				</span>
                            <h6 class="_600 m-y">SASS Css</h6>
                            <p class="text-muted">its source code utilizes Sass, a popular CSS preprocessor. Quickly get started with precompiled CSS or build on the source.</p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-social-nodejs text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Bower &amp; Grunt</h6>
                            <p class="text-muted">Automate development tasks, like compiling less to css, concatenating and minifying js files; Bower manages frameworks, libraries, assets, and utilities for you.</p>
                        </div>
                        <div class="col-sm-4 m-y-md">
				<span class="text-muted">
					<i class="ion-social-chrome-outline text-3x"></i>
				</span>
                            <h6 class="_600 m-y">Cross Browser</h6>
                            <p class="text-muted">Supports a wide variety of modern browsers and devices, built to work best in the latest desktop and mobile browsers</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dark-light" id="demos">
                <div class="container">
                    <div class="p-a-lg text-center">
                        <h2 class="m-b-lg">Что нужно знать перед началом работы</h2>
                        <p>
                            <a href="dashboard.html?bg=&folded=true" class="btn info">Light Theme</a>
                            <a href="dashboard.html?bg=black&folded=true" class="btn black">Dark Theme</a>
                            <a href="rtl.dashboard.html?bg=&folded=true" class="btn success">RTL</a>
                        </p>
                    </div>
                </div>
                <div class="no-scroll h-v" style="max-height: 500px">
                    <div class="p-a"  style="transform: translate(50%, 0%) scale(.9) rotateX(60deg) rotateY(0deg) rotate(45deg);transform-origin: top;">
                        <div class="row">
                            <div class="col-xs-4">
                                <div>
                                    <div class="item-media item-media-4by3 margin">
                                    </div>
                                    <div class="item-media item-media-4by3 margin">
                                    </div>
                                    <a href="dashboard.html?bg=black" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/dark.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg info">
                                            <div class="hidden-sm-down">
                                                <h4>Dark Theme</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="app.contact.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/contact.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg success">
                                            <div class="hidden-sm-down">
                                                <h4>Contacts App</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div>
                                    <div class="item-media item-media-4by3 margin">
                                    </div>
                                    <a href="chartjs.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/chart.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg accent">
                                            <div class="hidden-sm-down">
                                                <h4>Beautiful Chart</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="dashboard.html?bg=" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/dashboard.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg warn">
                                            <div class="hidden-sm-down">
                                                <h4>Light Theme</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="app.message.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/message.png') }});background-size: cover">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg indigo">
                                            <div class="hidden-sm-down">
                                                <h4>Project Management</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="app.project.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/project.png') }});background-size: cover">
                                    </a>
                                    <a href="map.vector.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/map.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg danger">
                                            <div class="hidden-sm-down">
                                                <h4>World Market</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="app.inbox.html" class="item-media item-media-4by3 margin box-shadow-z3" style="background-image: url({{ URL::asset('img/demos/inbox.png') }});background-size: cover">
                                    </a>
                                    <div class="item-media item-media-4by3 margin box-shadow-z3">
                                        <div class="item-media-content p-a-lg dark-white">
                                            <div class="hidden-sm-down">
                                                <h4>Message App</h4>
                                                <p class="text-muted">Config any color on any blocks. with Grey, Dark, Black Themes</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->

    <div class="footer black text-info-hover">
        <div class="container">
            <div class="p-y-lg b-b">
                <a href="{{ route('signUp') }}" class="btn info rounded pull-right pull-none-xs">Присоединиться</a>
                <div class="m-r">
                    <div class="text-md p-y-sm">Всё что нужно для тестирования в одной коробке!</div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row p-y-lg">
                <div class="col-md-5">
                    <h6 class="text-sm text-u-c m-b text-muted">I'm</h6>
                    <p>Modern, Clean &amp; Flat User Interface Kit, <br>Made with LOVE &amp; PASSION</p>
                    <p class="m-t-md text-muted p-r-lg">
                        <small class="text-muted">Пусть тайна останется тайной. ® Возвращение в море</small>
                    </p>
                    <div class="text-muted m-y-lg">
                        <h2 class="text-muted _600">
                            <span class="text-muted">@if(isset($logo)) {{ $logo }} @endif</span>
                        </h2>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <h6 class="text-sm text-u-c m-b text-muted">Company</h6>
                    <div class="m-b-md">
                        <ul class="nav l-h-2x">
                            <li class="nav-item">
                                <a class="nav-link" href="#">О нас</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Блог</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Вакансии</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Помощь</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Terms &amp; Policy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Cookies</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <h6 class="text-sm text-u-c m-b text-muted">Приложения</h6>
                    <div class="m-b-md">
                        <ul class="nav l-h-2x">
                            <li class="nav-item">
                                <a class="nav-link" href="app.project.html">Проекты</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="app.inbox.html">Почтовый ящик</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="app.message.html">Мессенджер</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="app.contact.html">Контакты</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <h6 class="text-sm text-u-c m-b text-muted">Связь</h6>
                    <div class="m-b-md">
                        <ul class="nav l-h-2x">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Контакты</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Поддержка</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="p-y-lg">
                <div class="m-b-lg text-sm">

                    <div class="text-muted pull-right pull-none-xs m-b">
                        <span class="text-muted">&copy; Copyright. All rights reserved.</span>
                    </div>
                    <div>
                        <a href="#" class="btn btn-sm btn-icon btn-social rounded lt" title="Facebook">
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-facebook indigo"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-icon btn-social rounded lt" title="Twitter">
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-twitter light-blue"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-icon btn-social rounded lt" title="Google+">
                            <i class="fa fa-google-plus"></i>
                            <i class="fa fa-google-plus red-600"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
